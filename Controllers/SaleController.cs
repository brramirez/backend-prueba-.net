﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Prueba.Context;
using Prueba.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Prueba.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SaleController : ControllerBase
    {
        private readonly shopContext context;

        public SaleController(shopContext context)
        {
            this.context = context;
        }
        // GET: api/<ValuesController>
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                return Ok(context.Sales.Include("SaleUser").ToList());
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        // GET api/<ValuesController>/5
        [HttpGet("{id}", Name = "GetSale")]
        public ActionResult Get(int id)
        {
            try
            {
                var sale = context.Sales.Include("SaleUser").Include("SaleProducts.SaleProductsProduct").FirstOrDefault(f => f.SaleId == id);
                return Ok(sale);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);

            }
        }



        // POST api/<ValuesController>
        [HttpPost]
        public ActionResult Post([FromBody] Sale sale)
        {
            try
            {
                context.Sales.Add(sale);
                context.SaveChanges();

                return CreatedAtRoute("GetSale", new { id = sale.SaleId }, sale);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);

            }
        }

        // POST api/<ValuesController>
        [HttpPost("items")]
        public ActionResult Post([FromBody] SaleProduct salesProduct)
        {
            try
            {
                
                context.SaleProducts.Add(salesProduct);
                context.SaveChanges();
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.InnerException);

            }
        }

        // PUT api/<ValuesController>/5
        [HttpPost("{id}/update")]
        public ActionResult Put(int id, [FromBody] Sale sale)
        {
            try
            {
                if (sale.SaleId == id)
                {
                    context.Entry(sale).State = EntityState.Modified;
                    context.SaveChanges();
                    return CreatedAtRoute("GetUser", new { id = sale.SaleId }, sale);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);

            }
        }

        // DELETE api/<ValuesController>/5
        [HttpPost("{id}/delete")]
        public ActionResult Delete(int id)
        {
            try
            {
                var sale = context.Sales.FirstOrDefault(f => f.SaleId == id);
                if (sale != null)
                {
                    context.Sales.Remove(sale);
                    context.SaveChanges();
                    return Ok(id);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);

            }
        }
    }
}

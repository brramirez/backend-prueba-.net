﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Prueba.Context;
using Prueba.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Prueba.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {

        private readonly shopContext context;

        public ProductController(shopContext context)
        {
            this.context = context;
        }
        // GET: api/<ProductController>
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                return Ok(context.Products.ToList());
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);

            }
        }

        // GET api/<ProductController>/5
        [HttpGet("{id}", Name = "GetProduct")]
        public ActionResult Get(int id)
        {
            try
            {
                var product = context.Products.FirstOrDefault(f => f.ProductId == id);
                return Ok(product);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);

            }
        }

        // POST api/<ProductController>
        [HttpPost]
        public ActionResult Post([FromBody] Product product)
        {
            try
            {
                context.Products.Add(product);
                context.SaveChanges();
                return CreatedAtRoute("GetProduct", new { id = product.ProductId }, product);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);

            }
        }

        // PUT api/<ProductController>/5
        [HttpPost("{id}/update")]
        public ActionResult Put(int id, [FromBody] Product product)
        {
            try
            {
                if (product.ProductId == id)
                {
                    context.Entry(product).State = EntityState.Modified;
                    context.SaveChanges();
                    return CreatedAtRoute("GetProduct", new { id = product.ProductId }, product);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);

            }
        }

        // DELETE api/<ProductController>/5
        [HttpPost("{id}/delete")]
        public ActionResult Delete(int id)
        {
            try
            {
                var product = context.Products.FirstOrDefault(f => f.ProductId == id);
                if (product != null)
                {
                    context.Products.Remove(product);
                    context.SaveChanges();
                    return Ok(id);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);

            }
        }
    }
}

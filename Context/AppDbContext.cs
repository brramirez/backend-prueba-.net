﻿using Microsoft.EntityFrameworkCore;
using Prueba.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prueba.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            /*modelBuilder.Entity<Sale>()
                .HasMany(p => p.products)
                .WithMany(s => s.sales)
                .UsingEntity<SaleProduct>(
                    sp => sp.HasOne(prop => prop.product)
                    .WithMany()
                    .HasForeignKey(prop => prop.sale_products_product_id),
                     sp => sp.HasOne(prop => prop.sale)
                    .WithMany()
                    .HasForeignKey(prop => prop.sale_products_sale_id),
                     sp =>
                     {
                         sp.Property(prop => prop.sale_products_qty).HasDefaultValue(1);
                         sp.HasKey(prop => new { prop.sale_products_sale_id, prop.sale_products_product_id });
                     }

                );*/
        }

        public DbSet<Product> products { get; set; }
        public DbSet<User> users { get; set; }
        public DbSet<Sale> sales { get; set; }
        public DbSet<SaleProduct> sale_products { get; set; }
    }
}

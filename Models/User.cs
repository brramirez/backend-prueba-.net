﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Prueba.Models
{
    public partial class User
    {
        public User()
        {
            Sales = new HashSet<Sale>();
        }

        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserLastName { get; set; }
        public string UserNumIdent { get; set; }
        public DateTime UserBirthDate { get; set; }
        public string UserPhone { get; set; }

        public virtual ICollection<Sale> Sales { get; set; }
    }
}

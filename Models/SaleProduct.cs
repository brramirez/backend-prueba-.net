﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Prueba.Models
{
    public partial class SaleProduct
    {
        public int SaleProductsSaleId { get; set; }
        public int SaleProductsProductId { get; set; }
        public int SaleProductsQty { get; set; }
        public decimal? SaleProductsUnitPrice { get; set; }

        public virtual Product SaleProductsProduct { get; set; }
        public virtual Sale SaleProductsSale { get; set; }
    }
}

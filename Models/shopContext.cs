using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Prueba.Models
{
    public partial class shopContext : DbContext
    {
        public shopContext()
        {
        }

        public shopContext(DbContextOptions<shopContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Sale> Sales { get; set; }
        public virtual DbSet<SaleProduct> SaleProducts { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("data source=DESKTOP-BTPUTGQ;initial catalog=shop; user id=; password=;MultipleActiveResultSets=true");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Latin1_General_CI_AI");

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("products");

                entity.Property(e => e.ProductId).HasColumnName("product_id");

                entity.Property(e => e.ProductDescription)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("product_description");

                entity.Property(e => e.ProductName)
                    .IsRequired()
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("product_name");

                entity.Property(e => e.ProductPrice)
                    .HasColumnType("money")
                    .HasColumnName("product_price");

                entity.Property(e => e.ProductStock).HasColumnName("product_stock");
            });

            modelBuilder.Entity<Sale>(entity =>
            {
                entity.ToTable("sales");

                entity.Property(e => e.SaleId).HasColumnName("sale_id");

                entity.Property(e => e.SaleAmount).HasColumnName("sale_amount");

                entity.Property(e => e.SaleDateSale)
                    .HasColumnType("datetime")
                    .HasColumnName("sale_date_sale");

                entity.Property(e => e.SaleUserId).HasColumnName("sale_user_id");

                entity.HasOne(d => d.SaleUser)
                    .WithMany(p => p.Sales)
                    .HasForeignKey(d => d.SaleUserId)
                    .HasConstraintName("FK__sales__sale_user__276EDEB3");
            });

            modelBuilder.Entity<SaleProduct>(entity =>
            {
                entity.HasKey(e => new { e.SaleProductsSaleId, e.SaleProductsProductId })
                    .HasName("PK__sale_pro__D200DC180E4DABA9");

                entity.ToTable("sale_products");

                entity.Property(e => e.SaleProductsSaleId).HasColumnName("sale_products_sale_id");

                entity.Property(e => e.SaleProductsProductId).HasColumnName("sale_products_product_id");

                entity.Property(e => e.SaleProductsQty).HasColumnName("sale_products_qty");

                entity.Property(e => e.SaleProductsUnitPrice)
                    .HasColumnType("money")
                    .HasColumnName("sale_products_unit_price");

                entity.HasOne(d => d.SaleProductsProduct)
                    .WithMany(p => p.SaleProducts)
                    .HasForeignKey(d => d.SaleProductsProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__sale_prod__sale___2B3F6F97");

                entity.HasOne(d => d.SaleProductsSale)
                    .WithMany(p => p.SaleProducts)
                    .HasForeignKey(d => d.SaleProductsSaleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__sale_prod__sale___2A4B4B5E");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("users");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.UserBirthDate)
                    .HasColumnType("datetime")
                    .HasColumnName("user_birth_date");

                entity.Property(e => e.UserLastName)
                    .IsRequired()
                    .HasMaxLength(70)
                    .IsUnicode(false)
                    .HasColumnName("user_last_name");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(70)
                    .IsUnicode(false)
                    .HasColumnName("user_name");

                entity.Property(e => e.UserNumIdent)
                    .IsRequired()
                    .HasMaxLength(13)
                    .IsUnicode(false)
                    .HasColumnName("user_num_ident");

                entity.Property(e => e.UserPhone)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasColumnName("user_phone");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

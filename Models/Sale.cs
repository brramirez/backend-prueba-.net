﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Prueba.Models
{
    public partial class Sale
    {
        public Sale()
        {
            SaleProducts = new HashSet<SaleProduct>();
        }

        public int SaleId { get; set; }
        public int? SaleUserId { get; set; }
        public double? SaleAmount { get; set; }
        public DateTime? SaleDateSale { get; set; }

        public User SaleUser { get; set; }
        public ICollection<SaleProduct> SaleProducts { get; set; }
    }
}
